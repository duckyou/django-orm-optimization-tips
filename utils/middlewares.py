# -*- coding: utf-8 -*-
import time


def timeit(get_response):
    """"""

    # print('timeit initialization...')

    def middleware(request):

        # print('timeit before view')
        s = time.time()

        response = get_response(request)
        # print('timeit after view')
        print(f'timeit: {round((time.time() - s), 4)} s')

        return response

    return middleware
