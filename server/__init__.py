# -*- coding: utf-8 -*-

from pathlib import PurePath

from decouple import AutoConfig

# Build paths inside the project like this: BASE_DIR.joinpath('some')
BASE_DIR = PurePath(__file__).parent.parent

# Load .env files
# See docs: https://github.com/henriquebastos/python-decouple
config = AutoConfig(search_path=BASE_DIR.joinpath('server/config/.env'))
