# -*- coding: utf-8 -*-

from django.urls import path

from .. import apis


app_name = 'shop_v1'

urlpatterns = [
    # Products
    path('products/',
         apis.ProductsViewSet.as_view(
             {'get': 'list', 'post': 'create'},
         ),
         name='products',),
    path(
        'products/<pk>/',
        apis.ProductsViewSet.as_view(
            {'get': 'retrieve', 'put': 'update', 'delete': 'remove'},
        ),
        name='product',),
]
