# -*- coding: utf-8 -*-

from django.urls import path

from .. import apis


app_name = 'shop_v2'

urlpatterns = [
    # Products
    path('products/',
         apis.ProductsV2ViewSet.as_view(
             {'get': 'list', 'post': 'create'},
         ),
         name='products', ),
    path(
        'products/<pk>/',
        apis.ProductsV2ViewSet.as_view(
            {'get': 'retrieve', 'put': 'update', 'delete': 'remove'},
        ),
        name='product', ),
]
