# -*- coding: utf-8 -*-

from rest_framework import serializers

from ..models import Product, ProductStatus, Tag, Category


class OnlyNameRepresentationMixin:
    """"""
    def to_representation(self, instance):
        instance = super().to_representation(instance)
        return instance['name']


class ProductTagListSerializer(OnlyNameRepresentationMixin,
                               serializers.ModelSerializer):
    """"""
    class Meta:
        model = Tag
        fields = ('name',)


class ProductCategoryListSerializer(OnlyNameRepresentationMixin,
                                    serializers.ModelSerializer):
    """"""
    class Meta:
        model = Category
        fields = ('name',)


class ProductStatusSerializer(OnlyNameRepresentationMixin,
                              serializers.ModelSerializer):
    """"""
    class Meta:
        model = ProductStatus
        fields = ('name',)


class ProductListSerializer(serializers.ModelSerializer):
    """"""
    product_status = ProductStatusSerializer()

    class Meta:
        model = Product
        fields = ('id', 'name', 'description', 'price', 'product_status',)


class ProductSerializer(serializers.ModelSerializer):
    """"""
    product_status = ProductStatusSerializer()
    tags = ProductTagListSerializer(many=True)
    categories = ProductCategoryListSerializer(many=True)

    class Meta:
        model = Product
        exclude = ('id', 'inserted_at', 'updated_at',)
