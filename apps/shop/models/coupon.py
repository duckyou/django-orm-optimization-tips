# -*- coding: utf-8 -*-

from django.db import models
from django.core import validators
from django.utils.translation import gettext_lazy as _

from ._abstract import InsertedUpdatedFieldsModel


class Coupon(InsertedUpdatedFieldsModel):
    """"""
    code = models.CharField(
        unique=True,
        verbose_name=_('code'),
        # help_text=_(''),
        max_length=255,
    )
    description = models.TextField(
        verbose_name=_('description'),
        # help_text=_(''),
    )
    active = models.BooleanField(
        verbose_name=_('active'),
        # help_text=_(''),
        default=False,
    )
    multiple = models.BooleanField(
        verbose_name=_('multiple'),
        # help_text=_(''),
        default=False,
    )
    value = models.FloatField(
        verbose_name=_('value'),
        # help_text=_(''),
        default=0.0,
        validators=[
            validators.MinValueValidator(0.0),
            validators.MaxValueValidator(100.0),
        ],
    )
    start_datetime = models.DateTimeField(
        verbose_name=_('start datetime'),
        # help_text=_(''),
        auto_now_add=True,
    )
    end_datetime = models.DateTimeField(
        verbose_name=_('end datetime'),
        # help_text=_(''),
        null=True,
        default=None,
    )
