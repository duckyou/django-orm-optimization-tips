# -*- coding: utf-8 -*-

from django.db import models
from django.core import validators
from django.utils.translation import gettext_lazy as _


class InsertedUpdatedFieldsModel(models.Model):
    """"""
    class Meta:
        abstract = True

    inserted_at = models.DateTimeField(
        verbose_name=_('inserted at'),
        # help_text=_(''),
        auto_now_add=True,
        null=False,
    )
    updated_at = models.DateTimeField(
        verbose_name=_('updated at'),
        # help_text=_(''),
        auto_now=True,
        null=False,
    )


class ProductModel(InsertedUpdatedFieldsModel):
    """"""
    class Meta:
        abstract = True

    sku = models.CharField(
        verbose_name=_('stock keeping unit'),
        # help_text=_(''),
        max_length=255,
        null=False,
    )
    name = models.CharField(
        verbose_name=_('name'),
        # help_text=_(''),
        max_length=255,
        null=False,
        db_index=True,
    )
    description = models.TextField(
        verbose_name=_('description'),
        # help_text=_(''),
    )
    quantity = models.PositiveIntegerField(
        verbose_name=_('quantity'),
        # help_text=_(''),
    )
    price = models.DecimalField(
        verbose_name=_('regular price'),
        # help_text=_(''),
        validators=[
            validators.MinValueValidator(0.0),
        ],
        max_digits=14,
        decimal_places=2,
    )
    discount_price = models.DecimalField(
        verbose_name=_('discount price'),
        # help_text=_(''),
        validators=[
            validators.MinValueValidator(0.0),
        ],
        max_digits=14,
        decimal_places=2,
    )
