# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core import validators

from ._abstract import ProductModel
from .sale_order import SaleOrder


class OrderProduct(ProductModel):
    """"""
    order = models.ForeignKey(
        verbose_name=_('order'),
        # help_text=_(''),
        to=SaleOrder,
        on_delete=models.DO_NOTHING,
    )
    subtotal = models.DecimalField(
        verbose_name=_(''),
        # help_text=_(''),
        validators=[
            validators.MinValueValidator(0.0),
        ],
        max_digits=14,
        decimal_places=2,
    )
