# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core import validators

from ._abstract import InsertedUpdatedFieldsModel
from .sale_order import SaleOrder


class Transaction(InsertedUpdatedFieldsModel):
    """"""
    code = models.PositiveIntegerField(
        verbose_name=_('code'),
        # help_text=_(''),
        null=False,
    )
    transtime = models.DateTimeField(
        verbose_name=_('transaction datetime'),
        # help_text=_(''),
    )
    rrn = models.CharField(
        verbose_name=_('retrieval reference number'),
        # help_text=_(''),
        max_length=255,
    )
    order = models.ForeignKey(
        verbose_name=_('order'),
        # help_text=_(''),
        to=SaleOrder,
        on_delete=models.SET_NULL,
        null=True,
    )
    amount = models.DecimalField(
        verbose_name=_('amount'),
        # help_text=_(''),
        validators=[
            validators.MinValueValidator(0.0),
        ],
        max_digits=14,
        decimal_places=2,
    )
    cc_number = models.CharField(
        verbose_name=_('cc number'),
        # help_text=_(''),
        max_length=255,
    )
    cc_type = models.CharField(
        verbose_name=_('cc type'),
        # help_text=_(''),
        max_length=255,
    )
    response = models.TextField(
        verbose_name=_('response'),
        # help_text=_(''),
    )
