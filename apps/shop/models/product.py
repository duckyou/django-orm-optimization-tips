# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext_lazy as _

from ._abstract import ProductModel
from .product_status import ProductStatus
from .tag import Tag
from .category import Category


class Product(ProductModel):
    """"""
    product_status = models.ForeignKey(
        verbose_name=_('product_status'),
        # help_text=_(''),
        to=ProductStatus,
        on_delete=models.SET_NULL,
        null=True,
    )
    taxable = models.BooleanField(
        verbose_name=_('taxable'),
        # help_text=_(''),
    )
    tags = models.ManyToManyField(
        verbose_name=_('tags'),
        # help_text=_(''),
        to=Tag,
        related_name='products',
    )
    categories = models.ManyToManyField(
        verbose_name=_('categories'),
        # help_text=_(''),
        to=Category,
        related_name='products',
    )
