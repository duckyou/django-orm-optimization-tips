# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext_lazy as _

from ._abstract import InsertedUpdatedFieldsModel


class Tag(InsertedUpdatedFieldsModel):
    """"""
    name = models.CharField(
        verbose_name=_('tag'),
        # help_text=_(''),
        max_length=255,
    )
