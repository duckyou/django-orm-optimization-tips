# -*- coding: utf-8 -*-

from django.db import models
from django.core import validators
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

from ._abstract import InsertedUpdatedFieldsModel
from .coupon import Coupon

User = get_user_model()


class SaleOrder(InsertedUpdatedFieldsModel):
    """"""
    # or session if registration is needless
    user = models.ForeignKey(
        verbose_name=_('user'),
        # help_text=_(''),
        to=User,
        on_delete=models.SET_NULL,
        null=True,
    )
    order_date = models.DateField(
        verbose_name=_('order date'),
        # help_text=_(''),
    )
    total = models.DecimalField(
        verbose_name=_('total'),
        # help_text=_(''),
        validators=[
            validators.MinValueValidator(0.0),
        ],
        max_digits=14,
        decimal_places=2,
    )
    coupon = models.ForeignKey(
        verbose_name=_('coupon'),
        # help_text=_(''),
        to=Coupon,
        on_delete=models.DO_NOTHING,
        null=True,
    )
