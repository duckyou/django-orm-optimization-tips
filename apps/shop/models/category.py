# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext_lazy as _

from ._abstract import InsertedUpdatedFieldsModel


class Category(InsertedUpdatedFieldsModel):
    """"""
    name = models.CharField(
        verbose_name=_('name'),
        # help_text=_(''),
        max_length=255,
    )
    parent_category = models.ForeignKey(
        verbose_name=_('parent category'),
        # help_text=_(''),
        to="self",
        on_delete=models.CASCADE,
        null=True
    )
