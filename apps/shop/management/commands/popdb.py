# -*- coding: utf-8 -*-

import random
import time

from django.core.management import BaseCommand
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from mimesis.providers import Datetime

from ...models import (Category, Coupon, OrderProduct, Product, ProductStatus,
                       SaleOrder, Tag, Transaction,)

User = get_user_model()


def timeit(func):
    def wrapper(*args, **kwargs):
        s = time.time()
        result = func(*args, **kwargs)
        ms = (time.time() - s) * 1000
        print(f'{ms} ms')
        return result
    return wrapper


class Command(BaseCommand):
    """"""
    help = 'Populate shop database with mock data'

    def handle(self, *args, **options):
        if not settings.DEBUG:
            print('Only for debug purposes')
            return

        self._run()
        print('Shop database populated successfully.')

    def _run(self):
        self._create_users()
        self._create_categories()
        self._create_coupons()
        self._create_product_statuses()
        self._create_tags()
        self._create_sale_orders()
        self._create_transactions()
        self._create_order_products()
        self._create_products()

    @timeit
    def _create_users(self, count=100):
        """"""
        self._users = [
            User(
                is_staff=False,
                is_superuser=False,
                username=f'User {i}',
                email=f'user.{i}@lab.com',
                password=make_password('123456789'),
            )
            for i in range(count)
        ]

        User.objects.bulk_create(self._users)

        print('Created users...')

    @timeit
    def _create_categories(self):
        """"""
        def create_category(depth=5, parent=None, branches=2):
            if depth > 0:
                for branch in range(1, branches + 1):
                    create_category(
                        depth - 1,
                        Category.objects.create(
                            name=f'Category {depth}.{branch}',
                            parent_category=parent,
                        ),
                    )
        create_category()
        self._categories = Category.objects.all()

        print('Created categories...')

    @timeit
    def _create_coupons(self, count=10_000):
        """"""
        boolean_choices = [True, False]
        dtg = Datetime()

        self._coupons = [
            Coupon(
                code=f'code {i}',
                description='description of {i}',
                active=random.choice(boolean_choices),
                multiple=random.choice(boolean_choices),
                value=round(random.random() * 100, 2),
                start_datetime=dtg.datetime(),
            )
            for i in range(count)
        ]

        Coupon.objects.bulk_create(self._coupons)

        print('Created coupons...')

    @timeit
    def _create_product_statuses(self, count=10_000):
        """"""
        self._product_statuses = [
            ProductStatus(
                name=f'Product status {i}',
            )
            for i in range(count)
        ]

        ProductStatus.objects.bulk_create(self._product_statuses)

        print('Created product statuses...')

    @timeit
    def _create_tags(self, count=10_000):
        """"""
        self._tags = [
            Tag(name=f'tag{i}')
            for i in range(count)
        ]

        Tag.objects.bulk_create(self._tags)

        print('Created tags...')

    @timeit
    def _create_sale_orders(self, count=10_000):
        """"""
        dtg = Datetime()

        self._sale_orders = [
            SaleOrder(
                user=random.choice(self._users),
                order_date=dtg.date(),
                total=random.random() * random.randint(1, 99),
                coupon=random.choice(self._coupons),
            )
            for _ in range(count)
        ]

        SaleOrder.objects.bulk_create(self._sale_orders)

        print('Created sale orders...')

    @timeit
    def _create_transactions(self, count=10_000):
        """"""
        dtg = Datetime()
        transactions = []

        for i in range(count):
            sale_order = random.choice(self._sale_orders)
            transactions.append(Transaction(
                code=i,
                transtime=dtg.datetime(),
                rrn=f'rrn {i}',
                order=sale_order,
                amount=sale_order.total,
                cc_number=f'ccnum {i}',
                cc_type=f'cctype {i}',
                response=f'response {i}' * 5,
            ))

        Transaction.objects.bulk_create(transactions)

        print('Created transactions...')

    @timeit
    def _create_order_products(self, count=10_000):
        """"""
        order_products = []

        for i in range(count):
            sale_order = random.choice(self._sale_orders)
            quantity = random.randint(1, 10)

            order_products.append(OrderProduct(
                sku=f'sku {i}',
                name=f'Ordered product {i}',
                description=f'Description of ordered product {i}',
                quantity=quantity,
                price=sale_order.total / quantity,
                discount_price=sale_order.total * 0.75,
                order=sale_order,
                subtotal=sale_order.total,
            ))

        OrderProduct.objects.bulk_create(order_products)

        print('Created order products...')

    @timeit
    def _create_products(self, count=1_000_001):
        """"""
        ProductTags = Product.tags.through
        ProductCategories = Product.categories.through

        tf = [True, False]
        tags_len = len(self._tags) - 5
        categories_len = len(self._categories) - 5
        products = []

        s = time.time()
        for i in range(count):
            price = random.random() * random.randint(1, 9)

            products.append(Product(
                sku=f'sku {i}',
                name=f'Product {i}',
                description=f'Description of product {i}',
                quantity=random.randint(1, 99),
                price=price,
                discount_price=price * 0.75,
                product_status=random.choice(self._product_statuses),
                taxable=random.choice(tf),
            ))

            if i % 10000 == 0:
                # Create products
                Product.objects.bulk_create(
                    products,
                )

                product_tags = []
                product_categories = []
                t_random = random.randint(0, tags_len)
                c_random = random.randint(0, categories_len)

                for p in products:
                    pid = p.id
                    # wow such a python... so exciting...
                    # ps. don't hit me for this code (((
                    product_tags.extend([
                        ProductTags(product_id=pid, tag_id=tag.id)
                        for tag in self._tags[
                            t_random:t_random + random.randint(1, 5)
                        ]
                    ])
                    product_categories.extend(
                        ProductCategories(product_id=pid, category_id=c.id)
                        for c in self._categories[
                                 c_random:c_random + random.randint(1, 5)
                        ]
                    )

                # Create ProductTags
                ProductTags.objects.bulk_create(product_tags)
                # Create ProductCategories
                ProductCategories.objects.bulk_create(product_categories)

                print('10000 products created...')
                print('For {} ms'.format((time.time() - s) * 1000))
                products = []
                s = time.time()
        print('Created products...')
