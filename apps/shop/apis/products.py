# -*- coding: utf-8 -*-

import json

from django.db import connection
from django.db.models import Subquery
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework import status
from rest_framework.filters import SearchFilter
from rest_framework import serializers

from ..models import Product
from ..serializers import ProductListSerializer, ProductSerializer

ProductCategory = Product.categories.through
ProductTags = Product.tags.through


def connection_queries(func):
    def wrapper(*args, **kwargs):
        response = func(*args, **kwargs)
        print(json.dumps(connection.queries, indent=4))
        return response
    return wrapper


class ProductsViewSet(ViewSet):
    """"""
    class QPListSerializer(serializers.Serializer):
        product_status = serializers.ListField(
            child=serializers.IntegerField(),
            required=False,
        )
        category = serializers.ListField(
            child=serializers.IntegerField(),
            required=False,
        )

    search_fields = ('name', 'description',)

    def list(self, req: Request):
        """"""
        # Parse query parameters through serializer
        qp = self.QPListSerializer(dict(req.query_params)).data

        # Construct query
        products = (Product.objects
                    .only('id', 'name', 'description', 'price',
                          'product_status__name')
                    .select_related('product_status'))

        if 'product_status' in qp:
            products = products.filter(product_status__in=qp['product_status'])
        if 'category' in qp:
            cps = Subquery(ProductCategory
                           .objects
                           .filter(category_id__in=qp['category'])
                           .values('product_id'))
            products = products.filter(id__in=cps)

        filtered_products = SearchFilter().filter_queryset(
            self.request, products, self,
        )[:1000]

        print(filtered_products.query)

        # Hit database and serialize
        data = ProductListSerializer(filtered_products, many=True).data

        return Response(
            data=data,
            status=status.HTTP_200_OK,
        )

    def create(self, req: Request):
        """"""
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    def retrieve(self, req: Request, pk: int):
        """"""
        try:
            product = (Product.objects
                       .defer('inserted_at', 'updated_at')
                       .get(pk=pk))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = ProductSerializer(product).data

        return Response(data=data, status=status.HTTP_200_OK)

    def update(self, req: Request, **kwargs):
        """"""
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    def remove(self, req: Request, **kwargs):
        """"""
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)


class ProductsV2ViewSet(ViewSet):
    """"""
    class QPListSerializer(serializers.Serializer):
        """"""
        product_status = serializers.ListField(
            child=serializers.IntegerField(),
            required=False,
        )
        category = serializers.ListField(
            child=serializers.IntegerField(),
            required=False,
        )

    search_fields = ('name', 'description',)

    def list(self, req: Request):
        """"""
        # Parse query parameters through serializer
        qp = self.QPListSerializer(dict(req.query_params)).data

        # Construct raw query
        # products = (Product.objects
        #             .all()
        #             .only(
        #                 'id', 'name', 'description', 'price', 'product_status',
        #             )
        #             .prefetch_related('product_status'))
        # print(products)

        # if 'product_status' in qp:
        #     products = products.filter(product_status__in=qp['product_status'])
        # if 'category' in qp:
        #     category_products = (ProductCategory.objects
        #                          .filter(category_id__in=qp['category'])
        #                          .values_list('product_id', flat=True))
        #     products = products.filter(id__in=category_products)
        # filtered_products = SearchFilter().filter_queryset(
        #     self.request, products, self,
        # )[:1000]

        # with connection.cursor() as cursor:
        #     cursor.execute('SELECT '
        #                    '"shop_product"."id", '
        #                    '"shop_product"."name", '
        #                    '"shop_product"."description", '
        #                    '"shop_product"."price", '
        #                    '"shop_product"."product_status_id" '
        #                    'FROM "shop_product" LIMIT 1000')
        #     print(cursor.fetchall())

        # print(products.query)

        # Hit database and serialize
        # data = ProductListSerializer(products, many=True).data

        return Response(
            data={},
            status=status.HTTP_200_OK,
        )

    def create(self, req: Request):
        """"""
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    def retrieve(self, req: Request, pk: int):
        """"""
        try:
            product = (Product.objects
                       .defer('inserted_at', 'updated_at')
                       .get(pk=pk))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = ProductSerializer(product).data

        return Response(data=data, status=status.HTTP_200_OK)

    def update(self, req: Request, **kwargs):
        """"""
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)

    def remove(self, req: Request, **kwargs):
        """"""
        return Response(status=status.HTTP_501_NOT_IMPLEMENTED)
