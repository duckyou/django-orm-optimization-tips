# -*- coding: utf-8 -*-
import random

from locust import HttpLocust, TaskSet


API_V = 1


def products(l):
    """"""
    l.client.get(f'/v{API_V}/shop/products/')


def product(l):
    """"""
    product_id = random.randint(1, 1_000_000)

    l.client.get(f'/v{API_V}/shop/products/{product_id}/')


def products_search(l):
    """"""
    l.client.get(f'/v{API_V}/shop/products/?search={random.randint(1, 1_000_000)}')


def products_filter(l):
    """"""
    category_id = random.randint(1, 1000)
    product_status_id = random.randint(1, 1000)

    l.client.get(f'/v{API_V}/shop/products/'
                 f'?category={category_id}&product_status={product_status_id}')


class UserBehavior(TaskSet):
    tasks = {
        products: 1,
        product: 5,
        products_search: 2,
        products_filter: 2,
    }


class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000
